# -*- encoding: utf-8 -*-
from __future__ import print_function
import peewee as p

db = p.SqliteDatabase('ghibli.db')


class Film(p.Model):
    title = p.CharField()
    description = p.CharField()
    director = p.CharField()
    producer = p.CharField()
    release_date = p.IntegerField()
    rt_score = p.IntegerField()

    class Meta:
        database = db
        db_table = "films"

    def full_title(self):
        t = self.title
        if self.release_date:
            t = '{} [{}]'.format(t, self.release_date)
        return t


class Vehicle(p.Model):
    name = p.CharField()
    description = p.CharField()
    vehicle_class = p.CharField()
    length = p.FloatField()
    film = p.ForeignKeyField(Film, related_name="vehicles")

    class Meta:
        database = db
        db_table = "vehicles"

print("Tutti i film dal più recente al più vecchio")
print("-------------------------------------------")
for f in Film.select().order_by(Film.release_date.desc(), Film.title):
    print(f.id, f.full_title())

print("")
print("Tutti i veicoli e il film dove appaiano")
print("---------------------------------------")
qs = Vehicle.select(Vehicle, Film)\
    .join(Film)
for v in qs:
    print(v.name, v.film.title)
