CREATE TABLE films(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	title TEXT NOT NULL,
	description TEXT NOT NULL,
	director TEXT NOT NULL,
	producer TEXT NOT NULL,
	release_date INTEGER NOT NULL,
	rt_score INTEGER NOT NULL
);

CREATE TABLE the_movie_db(
	film_id INTEGER PRIMARY KEY,
	link TEXT NOT NULL,
	image_url TEXT NOT NULL,
	FOREIGN KEY(film_id) REFERENCES films(id)
);

CREATE TABLE species(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	classification TEXT NOT NULL,
	eye_colors TEXT NOT NULL,
	hair_colors TEXT NOT NULL
);

CREATE TABLE people(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	gender TEXT NOT NULL,
	age TEXT NOT NULL,
	eye_color TEXT NOT NULL,
	hair_color TEXT NOT NULL,
	species_id INTEGER NOT NULL,
	FOREIGN KEY(species_id) REFERENCES species(id)
);

CREATE TABLE people_films(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	film_id INTEGER NOT NULL,
	people_id INTEGER NOT NULL,
	FOREIGN KEY(film_id) REFERENCES films(id),
	FOREIGN KEY(people_id) REFERENCES people(id)
);

CREATE TABLE locations(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	climate TEXT NOT NULL,
	terrain TEXT NOT NULL,
	surface_water INTEGER NOT NULL
);

CREATE TABLE locations_films(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	film_id INTEGER NOT NULL,
	location_id INTEGER NOT NULL,
	FOREIGN KEY(film_id) REFERENCES films(id),
	FOREIGN KEY(location_id) REFERENCES locations(id)
);

CREATE TABLE locations_residents(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	location_id INTEGER NOT NULL,
	people_id INTEGER NOT NULL,
	FOREIGN KEY(location_id) REFERENCES locations(id),
	FOREIGN KEY(people_id) REFERENCES people(id)
);

CREATE TABLE vehicles(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	description TEXT NOT NULL,
	vehicle_class TEXT NOT NULL,
	length REAL NOT NULL,
	pilot_id INTEGER NOT NULL,
	film_id INTEGER NOT NULL,
	FOREIGN KEY(film_id) REFERENCES films(id),
	FOREIGN KEY(pilot_id) REFERENCES people(id)
);
