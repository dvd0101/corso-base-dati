import json
import sqlite3
import sys


def load_films(cursor, films):
    sql = '''
    insert into films(title, description, director, producer, release_date, rt_score)
    values(?, ?, ?, ?, ?, ?);
    '''

    result = {}
    for row in films:
        params = (
            row['title'],
            row['description'],
            row['director'],
            row['producer'],
            int(row['release_date']),
            int(row['rt_score']),
        )
        cursor.execute(sql, params)

        cursor.execute('SELECT last_insert_rowid()')
        result[row['url']] = cursor.fetchall()[0][0]

    return result


def load_species(cursor, species):
    sql = '''
    insert into species(name, classification, eye_colors, hair_colors)
    values(?, ?, ?, ?);
    '''

    result = {}
    for row in species:
        params = (
            row['name'],
            row['classification'],
            row['eye_colors'],
            row['hair_colors'],
        )
        cursor.execute(sql, params)

        cursor.execute('SELECT last_insert_rowid()')
        result[row['url']] = cursor.fetchall()[0][0]

    return result


def load_people(cursor, people, films, species):
    sql_people = '''
    insert into people(name, gender, age, eye_color, hair_color, species_id)
    values(?, ?, ?, ?, ?, ?);
    '''
    sql_film = '''
    insert into people_films(film_id, people_id)
    values(?, ?);
    '''

    result = {}
    for row in people:
        params = (
            row['name'],
            row['gender'],
            row['age'],
            row['eye_color'],
            row['hair_color'],
            species[row['species']],
        )
        cursor.execute(sql_people, params)

        cursor.execute('SELECT last_insert_rowid()')
        result[row['url']] = person = cursor.fetchall()[0][0]

        for film in row['films']:
            params = (
                films[film],
                person,
            )
            cursor.execute(sql_film, params)

    return result


def load_locations(cursor, locations, films, people):
    sql_location = '''
    insert into locations(name, climate, terrain, surface_water)
    values(?, ?, ?, ?);
    '''
    sql_resident = '''
    insert into locations_residents(location_id, people_id)
    values(?, ?);
    '''
    sql_film = '''
    insert into locations_films(film_id, location_id)
    values(?, ?);
    '''
    result = {}
    for row in locations:
        params = (
            row['name'],
            row['climate'],
            row['terrain'],
            int(row['surface_water'] or '0'),
        )

        cursor.execute(sql_location, params)

        cursor.execute('SELECT last_insert_rowid()')
        result[row['url'][0]] = loc_id = cursor.fetchall()[0][0]

        for resident in filter(lambda x: x != 'TODO', row['residents']):
            params = (
                loc_id,
                people[resident],
            )
            cursor.execute(sql_resident, params)

        for film in row['films']:
            params = (
                films[film],
                loc_id,
            )
            cursor.execute(sql_film, params)

    return result


def load_vehicles(cursor, vehicles, films, people):
    sql = '''
    insert into vehicles(name, description, vehicle_class, length, pilot_id, film_id)
    values(?, ?, ?, ?, ?, ?);
    '''

    result = {}
    for row in vehicles:
        params = (
            row['name'],
            row['description'],
            row['vehicle_class'],
            float(row['length'].replace(',', '')),
            people[row['pilot']],
            films[row['films']],
        )
        cursor.execute(sql, params)

        cursor.execute('SELECT last_insert_rowid()')
        result[row['url']] = cursor.fetchall()[0][0]

    return result


def load_dataset(connection, dataset):
    cursor = connection.cursor()
    films = load_films(cursor, dataset['films'])
    species = load_species(cursor, dataset['species'])
    people = load_people(cursor, dataset['people'], films, species)
    load_locations(cursor, dataset['locations'], films, people)
    load_vehicles(cursor, dataset['vehicles'], films, people)

if __name__ == '__main__':
    with file(sys.argv[1]) as f:
        dataset = json.load(f)

    with sqlite3.connect(sys.argv[2]) as conn:
        load_dataset(conn, dataset)
