from flask import Flask, g, request
from flask import render_template
import sqlite3
app = Flask(__name__)


def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect("./ghibli.db")
    rv.row_factory = sqlite3.Row
    return rv


def db():
    if not hasattr(g, "db_connection"):
        g.db_connection = connect_db()
    return g.db_connection


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, "db_connection"):
        g.db_connection.close()


def query(sql, params=None):
    connection = db()
    cursor = connection.cursor()
    cursor.execute(sql, params or [])
    return cursor.fetchall()


@app.route("/")
def films():
    # 1 - Scrivere una query che estragga id, titolo, release_date e director
    # di ogni film
    # 2 - eseguire la query (usando la funzione `query()`) e passare il
    # risultato al template sotto la chiave "films"
    order_field = request.args.get("sort", "")
    if order_field not in ("title", "release_date", "director"):
        order_field = "title"
    order_direction = request.args.get("dir", "")
    if order_direction not in ("asc", "desc"):
        order_direction = "asc"

    sql = """
    select id, title, release_date, director
    from films
    order by %s %s
    """

    ctx = {
        "films": query(sql % (order_field, order_direction)),
        "order_field": order_field,
        "order_direction": order_direction,
    }
    return render_template("films.html", **ctx)


@app.route("/films/<film_id>/")
def film(film_id):
    # 1 - Scrivere una query che estragga tutti i dati del film identificato da
    # `film_id` compresi il link a the movie db e l'url dell'immagine, il
    # risultato deve essere accessibile dal template sotto la chiave "film"
    #
    # 2 - Scrivere una query che estragga id e nome delle location collegate al
    # film, rendere accessibile sotto la chiave "locations"
    #
    # 3 - Scrivere una query che estragga id e nome dei personaggi collegati al
    # film, rendere accessibile sotto la chiave "people"
    #
    # 4 - Scrivere una query che estragga i dati del veicolo (compreso nome ed
    # id del pilota), rendere accessibile sotto la chiave "vehicle"
    sql_film = """
    select films.*, the_movie_db.link, the_movie_db.image_url
    from films left join the_movie_db
        on films.id = the_movie_db.film_id
    where films.id = ?
    """

    sql_locations = """
    select locations.id, locations.name
    from locations inner join locations_films
        on locations.id = locations_films.location_id
    where locations_films.film_id = ?
    """

    sql_people = """
    select people.id, people.name
    from people inner join people_films
        on people.id = people_films.people_id
    where people_films.film_id = ?
    """

    sql_vehicle = """
    select
        v.name,
        v.description,
        v.vehicle_class,
        v.length,
        p.id as person_id,
        p.name as person
    from vehicles v inner join people p
        on v.pilot_id = p.id
    where v.film_id = ?
    """
    try:
        vehicle = query(sql_vehicle, [film_id])[0]
    except IndexError:
        vehicle = None
    ctx = {
        "film": query(sql_film, [film_id])[0],
        "locations": query(sql_locations, [film_id]),
        "people": query(sql_people, [film_id]),
        "vehicle": vehicle,
    }
    return render_template("film.html", **ctx)


@app.route("/people/")
def people():
    # 1 - Scrivere una query che estragga id, nome, nome della specie e numero
    # di film per ogni personaggio, rendere accessibile sotto la chiave
    # "people"
    order_field = request.args.get("sort", "")
    if order_field == "species":
        sort_column = "species.name"
    elif order_field == "films":
        sort_column = "count(people_films.id)"
    else:
        sort_column = "people.name"
    order_direction = request.args.get("dir", "")
    if order_direction not in ("asc", "desc"):
        order_direction = "asc"

    sql = """
    select
        people.id,
        people.name,
        species.name as species,
        count(people_films.id) as films
    from people left join people_films
        on people.id = people_films.people_id
    inner join species
        on people.species_id = species.id
    group by people.id, people.name, species.name
    order by %s %s
    """

    ctx = {
        "people": query(sql % (sort_column, order_direction,)),
        "order_field": order_field,
        "order_direction": order_direction,
    }
    return render_template("people.html", **ctx)


@app.route("/people/<people_id>/")
def person(people_id):
    # 1 - Scrivere una query che estragga i dati di una persona (identificata
    # da `people_id`) insieme al nome e alla classificazione della specie,
    # rendere accessibile sotto la chiave "person"
    #
    # 2 - Scrivere una query che estragga id e titolo dei film collegati alla
    # persona, rendere accessibile sotto la chiave "films"
    #
    # 3 - Scrivere una query che estragga id e nome delle location collegate alla
    # persona, rendere accessibile sotto la chiave "locations"
    sql_person = """
    select
        p.name,
        p.gender,
        p.age,
        p.eye_color,
        p.hair_color,
        s.name as species,
        s.classification
    from people p inner join species s
        on p.species_id = s.id
    where p.id = ?
    """

    sql_films = """
    select films.id, films.title
    from films inner join people_films
        on films.id = people_films.film_id
    where people_films.people_id = ?
    """

    sql_locations = """
    select locations.id, locations.name
    from locations inner join locations_residents
        on locations.id = locations_residents.location_id
    where locations_residents.people_id = ?
    """
    ctx = {
        "person": query(sql_person, [people_id])[0],
        "films": query(sql_films, [people_id]),
        "locations": query(sql_locations, [people_id]),
    }
    return render_template("person.html", **ctx)


@app.route("/locations/")
def locations():
    # 1 - Scrivere una query che estragga id, nome, numero di films e numero di
    # residenti per ogni location, rendere accessibile sotto la chiave
    # "locations"
    order_field = request.args.get("sort", "")
    if order_field == "residents":
        sort_column = "count(lr.id)"
    elif order_field == "films":
        sort_column = "count(lf.id)"
    else:
        sort_column = "l.name"
    order_direction = request.args.get("dir", "")
    if order_direction not in ("asc", "desc"):
        order_direction = "asc"

    sql = """
    select
        l.id,
        l.name,
        count(distinct lf.id) as films,
        count(distinct lr.id) as residents
    from locations l left join locations_films lf
        on l.id = lf.location_id
    left join locations_residents lr
        on l.id = lr.location_id
    group by l.id, l.name
    order by %s %s
    """

    ctx = {
        "locations": query(sql % (sort_column, order_direction)),
        "order_field": order_field,
        "order_direction": order_direction,
    }
    return render_template("locations.html", **ctx)


@app.route("/locations/<location_id>/")
def location(location_id):
    # 1 - Scrivere una query che estragga i dati di una location (identificata
    # da `location_id`), rendere accessibile sotto la chiave "location"
    #
    # 2 - Scrivere una query che estragga id e titolo dei film collegati alla
    # location, rendere accessibile sotto la chiave "films"
    #
    # 3 - Scrivere una query che estragga id e nome delle persone collegate alla
    # location, rendere accessibile sotto la chiave "people"
    sql_location = """
    select
        name,
        climate,
        terrain,
        surface_water
    from locations
    where id = ?
    """

    sql_films = """
    select films.id, films.title
    from films inner join locations_films
        on films.id = locations_films.film_id
    where locations_films.location_id = ?
    """

    sql_people = """
    select people.id, people.name
    from people inner join locations_residents
        on people.id = locations_residents.people_id
    where locations_residents.location_id = ?
    """
    ctx = {
        "location": query(sql_location, [location_id])[0],
        "films": query(sql_films, [location_id]),
        "people": query(sql_people, [location_id]),
    }
    return render_template("location.html", **ctx)
