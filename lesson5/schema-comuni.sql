create table regioni(
	id text primary key,
	nome text not null
);

create table province(
	id text primary key,
	regione_id text not null,
	nome text not null,
	FOREIGN KEY(regione_id) REFERENCES regioni(id)
);

create table comuni(
	id text primary key,
	nome text not null,
	codice_fisco text not null,
	provincia_id text not null,
	abitanti integer not null,
	cap text not null,
	prefisso text not null,
	link text not null,
	FOREIGN KEY(provincia_id) REFERENCES province(id)
);
