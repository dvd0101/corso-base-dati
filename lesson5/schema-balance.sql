CREATE TABLE accounts(
	name text primary key not null,
	balance integer not null
);

CREATE TABLE transfers(
	id integer primary key autoincrement,
	from_account text not null references accounts(name),
	to_account text not null references accounts(name),
	amount integer not null,
	completed timestamp null
);

PRAGMA foreign_keys = true;

insert into accounts(name, balance) values('Alice', 1000);
insert into accounts(name, balance) values('Bob', 500);
insert into accounts(name, balance) values('Carl', 0);
insert into accounts(name, balance) values('Dan', 300);
insert into transfers(from_account, to_account, amount) values('Alice', 'Bob', 100);
insert into transfers(from_account, to_account, amount) values('Dan', 'Carl', 10);
