# -*- encoding: utf-8 -*-
import csv
import sqlite3

regioni = {
    "ABR": "Abruzzo",
    "BAS": "Basilicata",
    "CAL": "Calabria",
    "CAM": "Campania",
    "EMR": "Emilia-Romagna",
    "FVG": "Friuli-Venezia Giulia",
    "LAZ": "Lazio",
    "LIG": "Liguria",
    "LOM": "Lombardia",
    "MAR": "Marche",
    "MOL": "Molise",
    "PIE": "Piemonte",
    "PUG": "Puglia",
    "SAR": "Sardegna",
    "SIC": "Sicilia",
    "TOS": "Toscana",
    "TAA": "Trentino-Alto Adige",
    "UMB": "Umbria",
    "VDA": "Valle d'Aosta",
    "VEN": "Veneto",
}

province = {
    "AG": u"Agrigento",
    "AL": u"Alessandria",
    "AN": u"Ancona",
    "AO": u"Valle d\'Aosta",
    "AP": u"Ascoli Piceno",
    "AQ": u"L\'Aquila",
    "AR": u"Arezzo Arezzo",
    "AT": u"Asti Asti",
    "AV": u"Avellino",
    "BA": u"Bari",
    "BG": u"Bergamo",
    "BI": u"Biella",
    "BL": u"Belluno",
    "BN": u"Benevento",
    "BO": u"Bologna",
    "BR": u"Brindisi",
    "BS": u"Brescia",
    "BT": u"Barletta-Andria-Trani",
    "BZ": u"Bolzano",
    "CA": u"Cagliari",
    "CB": u"Campobasso",
    "CE": u"Caserta",
    "CH": u"Chieti",
    "CI": u"Carbonia-Iglesias",
    "CL": u"Caltanissetta",
    "CN": u"Cuneo",
    "CO": u"Como",
    "CR": u"Cremona",
    "CS": u"Cosenza",
    "CT": u"Catania",
    "CZ": u"Catanzaro",
    "EN": u"Enna",
    "FC": u"Forlì-Cesena",
    "FE": u"Ferrara",
    "FG": u"Foggia",
    "FI": u"Firenze",
    "FM": u"Fermo",
    "FR": u"Frosinone",
    "GE": u"Genova",
    "GO": u"Gorizia",
    "GR": u"Grosseto",
    "IM": u"Imperia",
    "IS": u"Isernia",
    "KR": u"Crotone",
    "LC": u"Lecco",
    "LE": u"Lecce",
    "LI": u"Livorno",
    "LO": u"Lodi",
    "LT": u"Latina",
    "LU": u"Lucca",
    "MB": u"Monza e Brianza",
    "MC": u"Macerata",
    "ME": u"Messina",
    "MI": u"Milano",
    "MN": u"Mantova",
    "MO": u"Modena",
    "MS": u"Massa e Carrara",
    "MT": u"Matera",
    "NA": u"Napoli",
    "NO": u"Novara",
    "NU": u"Nuoro",
    "OG": u"Ogliastra",
    "OR": u"Oristano",
    "OT": u"Olbia-Tempio",
    "PA": u"Palermo",
    "PC": u"Piacenza",
    "PD": u"Padova",
    "PE": u"Pescara",
    "PG": u"Perugia",
    "PI": u"Pisa",
    "PN": u"Pordenone",
    "PO": u"Prato",
    "PR": u"Parma",
    "PT": u"Pistoia",
    "PU": u"Pesaro e Urbino",
    "PV": u"Pavia",
    "PZ": u"Potenza",
    "RA": u"Ravenna",
    "RC": u"Reggio Calabria",
    "RE": u"Reggio Emilia",
    "RG": u"Ragusa",
    "RI": u"Rieti",
    "RM": u"Roma",
    "RN": u"Rimini",
    "RO": u"Rovigo",
    "SA": u"Salerno",
    "SI": u"Siena",
    "SO": u"Sondrio",
    "SP": u"La Spezia",
    "SR": u"Siracusa",
    "SS": u"Sassari",
    "SV": u"Savona",
    "TA": u"Taranto",
    "TE": u"Teramo",
    "TN": u"Trento",
    "TO": u"Torino",
    "TP": u"Trapani",
    "TR": u"Terni",
    "TS": u"Trieste",
    "TV": u"Treviso",
    "UD": u"Udine",
    "VA": u"Varese",
    "VB": u"Verbano-Cusio-Ossola",
    "VC": u"Vercelli",
    "VE": u"Venezia",
    "VI": u"Vicenza",
    "VR": u"Verona",
    "VS": u"Medio Campidano",
    "VT": u"Viterbo",
    "VV": u"Vibo Valentia",
}

conn = sqlite3.connect('comuni.db')
cursor = conn.cursor()

for id, nome in regioni.items():
    cursor.execute('insert into regioni(id, nome) values(?, ?)', [id, nome])

sql_provincia = 'insert into province(id, regione_id, nome) values(?, ?, ?)'
sql = '''
insert into comuni(
    id, nome, codice_fisco,
    provincia_id, abitanti, cap,
    prefisso, link)
values(?, ?, ?, ?, ?, ?, ?, ?)
'''

province_aggiunte = set()
for row in csv.DictReader(file('listacomuni.txt'), delimiter=';'):
    if row['Provincia'] not in province_aggiunte:
        params = [
            row['Provincia'],
            row['Regione'],
            province[row['Provincia']],
        ]
        cursor.execute(sql_provincia, params)
        province_aggiunte.add(row['Provincia'])

    params = [
        unicode(row['Istat'], 'latin-1'),
        unicode(row['Comune'], 'latin-1'),
        unicode(row['CodFisco'], 'latin-1'),
        unicode(row['Provincia'], 'latin-1'),
        unicode(row['Abitanti'], 'latin-1'),
        unicode(row['CAP'], 'latin-1'),
        unicode(row['Prefisso'], 'latin-1'),
        unicode(row['Link'], 'latin-1'),
    ]
    cursor.execute(sql, params)

conn.commit()
