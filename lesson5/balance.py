import sqlite3
import sys
import time
from contextlib import contextmanager

try:
    transfer = int(sys.argv[1])
except IndexError:
    transfer = None

try:
    sleep_for = int(sys.argv[2])
except IndexError:
    sleep_for = None

conn = sqlite3.connect('balance.db', timeout=1)


@contextmanager
def transaction(conn):
    try:
        yield
    except sqlite3.OperationalError as e:
        if str(e) == "database is locked":
            print("database is locked, try again")
        conn.rollback()
        raise
    except Exception:
        conn.rollback()
        raise
    else:
        conn.commit()


def do_transfer(id):
    c = conn.cursor()
    with transaction(conn):
        c.execute('''
            select from_account, to_account, amount
            from transfers
            where id=? and completed is null
        ''', [id])
        rows = c.fetchall()
        if len(rows) == 0:
            # transfer not found
            return False
        from_ = rows[0][0]
        to_ = rows[0][1]
        amount = rows[0][2]

        c.execute("update accounts set balance = balance - ? where name = ?", [amount, from_])
        c.execute("update accounts set balance = balance + ? where name = ?", [amount, to_])
        c.execute("update transfers set completed=datetime('now') where id=?", [id])
        if sleep_for:
            print("sleeping for", sleep_for)
            time.sleep(sleep_for)
    return True


def show_accounts():
    c = conn.cursor()
    c.execute('select * from accounts order by name')
    rows = c.fetchall()
    print('Accounts')
    print('--------')
    for row in rows:
        print("{:8} {:>5}".format(*row))


if transfer:
    if not do_transfer(transfer):
        print("transfer already completed")
show_accounts()
