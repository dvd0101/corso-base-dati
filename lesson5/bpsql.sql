begin isolation level serializable;
select from_account, to_account, amount from transfers where id=1 and completed is null;
update accounts set balance = balance - 100 where name = 'Alice';
update accounts set balance = balance + 100 where name = 'Bob';
update transfers set completed=now() where id=1;

